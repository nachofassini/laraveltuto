<?php

namespace App\Http\Controllers;

use App\Auto;
use App\Http\Requests\AutosRequest;
use App\Events\AutoWasVisited;
use Illuminate\Http\Request;

class Autos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $autos = Auto::buscar($request)->paginate(10);
        return view('autos.index', compact('autos', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marcas = \App\Marca::get()->pluck('nombre', 'id')->toArray();
        return view('autos.create', compact('marcas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AutosRequest $request)
    {
        $auto = new Auto();
        $auto->nombre = $request->nombre;
        $auto->marca_id = $request->marca;
        $auto->image = $request->file('image')->storePublicly('autos');
        $auto->save();

        return redirect()->route('auto.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Auto $auto)
    {
        if (!session('auto_visited_' . $auto->id)) {
            event(new AutoWasVisited($auto));
            session(['auto_visited_' . $auto->id => true]);
        }
        return view('autos.show', compact('auto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Auto $auto)
    {
        $marcas = \App\Marca::get()->pluck('nombre', 'id')->toArray();
        return view('autos.edit', compact('auto', 'marcas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AutosRequest $request, Auto $auto)
    {
        $auto->nombre = $request->nombre;
        $auto->marca_id = $request->marca;
        $auto->image = $request->file('image')->storePublicly('autos');
        $auto->save();

        return redirect()->route('auto.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auto $auto)
    {
        $auto->delete();

        return redirect()->route('auto.index');
    }
}
