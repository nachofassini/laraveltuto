<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    public function autos()
    {
        return $this->hasMany(\App\Auto::class);
    }
}
