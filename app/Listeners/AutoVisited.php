<?php

namespace App\Listeners;

use App\Events\AutoWasVisited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoVisited
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutoWasVisited  $event
     * @return void
     */
    public function handle(AutoWasVisited $event)
    {
        $event->auto->visitar();
    }
}
