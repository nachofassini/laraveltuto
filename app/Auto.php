<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    public function marca()
    {
        return $this->belongsTo(\App\Marca::class);
    }

    public function scopeBuscar($query, $request)
    {
        if ($request->has('search')) {
            $search = '%' . $request->search . '%';
            return $query->where('nombre', 'like', $search)
                ->orWhereHas('marca', function ($query) use ($search) {
                    $query->where('nombre', 'like', $search);
                });
        }
    }

    function scopePorEdicion($query)
    {
        $query->orderBy('updated_at');
    }

    public
    function visitar()
    {
        $this->visitas++;
        $this->save();
    }
}
