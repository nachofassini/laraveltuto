@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="page-header">
        <h3>Agregar Autos</h3>
      </div>
      @section('formOpen')
        {!! Form::open(['route' => 'auto.store', 'enctype' => 'multipart/form-data']) !!}
      @show
      {!! Field::text('nombre', ['ph' => 'Nombre']) !!}
      {!! Field::select('marca', $marcas,['empty' => 'Marca']) !!}
      {!! Field::file('image') !!}
      <input class="btn btn-primary" type="submit" value="Guardar">
      <a class="btn btn-default" href="{{ route('auto.index') }}">Volver</a>
      {!! Form::close() !!}
    </div>
  </div>
@endsection
