@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="page-header">
        <h3>Autos</h3>
      </div>
      <ul>
        <li>{{ $auto->nombre }}</li>
        <li>{{ $auto->marca->nombre }}</li>
        <li><img src="{{ asset('storage/'.$auto->image) }}" alt="{{ asset('storage/'.$auto->image) }}"></li>
      </ul>
      <a class="btn btn-default" href="{{ route('auto.index') }}">Volver</a>
    </div>
  </div>
@endsection
