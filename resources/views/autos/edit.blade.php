@extends('autos.create')

@section('formOpen')
  {!! Form::model($auto, ['route' => ['auto.update', $auto->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
@endsection
