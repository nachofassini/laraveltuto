@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="page-header">
        <h3>Autos</h3>
      </div>
      <a class="btn btn-primary pull-right" href="{{ route('auto.create') }}">Nuevo auto</a>
      {!! Form::model($request, ['route' => 'auto.index', 'method' => 'get']) !!}
      {!! Field::text('search') !!}
      <input type="submit" value="buscar">
      {!! Form::close() !!}
      <table class="table table-responsive">
        <thead>
        <tr>
          <th></th>
          <th>Nombre</th>
          <th>Marca</th>
        </tr>
        </thead>
        <tbody>
        @foreach($autos as $auto)
          <tr>
            <td>
              <a class="btn btn-sm btn-default" href="{{ route('auto.show', $auto->id) }}">Ver</a>
              <a class="btn btn-sm btn-primary" href="{{ route('auto.edit', $auto->id) }}">Editar</a>
              {!! Form::open(['route' => ['auto.destroy', $auto->id], 'method' => 'delete']) !!}
              <input type="submit" class="btn btn-sm btn-danger" value="Eliminar">
              {!! Form::close() !!}
            </td>
            <td>{{ $auto->nombre }}</td>
            <td>{{ $auto->marca->nombre }}</td>
          </tr>
        @endforeach
        </tbody>
      </table>
      {!! $autos->links() !!}
    </div>
  </div>
@endsection
