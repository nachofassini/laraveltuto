<?php

use Illuminate\Database\Seeder;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Marca::class, 5)
            ->create()
            ->each(function ($marca) {
                $marca->autos()->saveMany(factory(\App\Auto::class, 5)->make());
            });
    }
}
